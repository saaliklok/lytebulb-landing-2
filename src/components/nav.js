import React from "react"
import { Link } from "gatsby"
import PropTypes from 'prop-types';

class Nav extends React.Component {
  state = {
    navOpen: 'navbar-menu',
  }

  toggleNav = () => {
      if(this.state.navOpen === 'navbar-menu'){
          this.setState({
              navOpen: 'navbar-menu is-active'
          })
      }
      else{
        this.setState({
            navOpen: 'navbar-menu'
        })
      }
  }

  render() {
    return (
      <nav className={this.props.navbar} role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <img src={this.props.logo} className="navLogo" />
          </Link>

          <div
            role="button"
            onClick={this.toggleNav}
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </div>
        </div>

        <div id="navbarBasicExample" className={this.state.navOpen}>
          <div className="navbar-start">
            <Link to="/" className="navbar-item">
              Projects
            </Link>
            <Link to="/about" className="navbar-item">
              About
            </Link>
            {/* <Link to="/consulting" className="navbar-item">
              Consulting
            </Link> */}
          </div>
        </div>
      </nav>
    )
  }
}

export default Nav

Nav.propTypes = {
  navbar: PropTypes.string.isRequired
}
