import React from "react"
import "../pages/global.scss"

class Footer extends React.Component {
  render() {
    return (
        <div className="footer footer-center">
          <div className="content">
            <p>&copy; Lytebulb 2019. All Rights Reserved.</p>
            <a className="button is-dark" href="https://lytebulb1.typeform.com/to/AsMExw" target="blank">
              Contact
            </a>
          </div>
        </div>
    )
  }
}

export default Footer
