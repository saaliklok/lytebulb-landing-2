import React from "react"
import PropTypes from "prop-types"

import CardPop from "../transitions/cardPop"
import "../sections/landingprojects/landingprojects.scss"

const ProjectCard = props => {
  let projectTags = props.tags
  return (
      <a target="blank" href={props.actionLink} className="no-underline">
        <div className="card">
          <div className="card-content">
            <div className="media">
              {/* <div class="media-left">
            <figure class="image is-48x48">
              <img
                src={props.imgsrc}
                alt="Placeholder image"
              />
            </figure>
          </div> */}

              <div className="media-content">
                <p className="title is-4">{props.title}</p>
                <p className="subtitle is-6">{props.tagline}</p>
                {projectTags.map((tag, index) => (
                  <span key={index} className="tag is-link">
                    {tag}
                  </span>
                ))}
              </div>
            </div>
          </div>
        </div>
      </a>
  )
}

ProjectCard.propTypes = {
  title: PropTypes.string.isRequired,
  tagline: PropTypes.string,
  actionLabel: PropTypes.string,
  actionLink: PropTypes.string,
  imgsrc: PropTypes.string,
}

export default ProjectCard
