import React from "react"
import "./about.scss"

import lytebulblogo from "../../../static/saalik.jpg"

const AboutLanding = () => {
  return (
    <div className="aboutContainer">
      <div className="container">
        <div className="content">
          <div className="section">
            <img src={lytebulblogo} className="logopic" alt="pic" />
            <p className="title is-4 light">
              Lytebulb is founded by{" "}
              <a href="https://saaliklok.com" target="blank">
                Saalik Lokhandwala
              </a>
              , a product manager and developer.
            </p>
          </div>
          <div className="section">
            <p className="title is-4 light">Hi! Welcome to Lytebulb.</p>
            <p>
              Lytebulb is a product studio creating software and content that helps dreamers
              and doers to be more mindful, present, and powerful.
            </p>
            {/* <p>
              Lytebulb is also a consulting business, assisting clients with a
              variety of technical projects: building apps from scratch,
              crafting impactful landing pages, and automating the boring bits
              of your business.
            </p> */}
          </div>
          <div className="section">
            <p className="title is-4 light">Who I Am</p>
            <p>
              My passion lies at the intersection of wellness (mental, physical,
              emotional, spiritual) and technology. Lytebulb is where I create
              products to make you do you, better.
            </p>
            <p>
              As a product manager, I manage the end to end process of creating
              a useful software product. I start with a problem that needs to be
              solved, craft a solution with software, and ensure it's
              financially feasible with business.
            </p>
            <p>
              I love to write, read, brainstorm, and build. As I continue on
              this journey, growing and changing, I'll be adding all sorts of
              projects to Lytebulb.
            </p>
            <p>
              Although I'm the solo "founder" of Lytebulb, my projects are highly
              collaborative. Many of them have co-founders, co-hosts, and
              collaborators. I lean on my network to create at a higher level
              than I could by myself. Nothing truly great is done alone.
            </p>
          </div>
          <div className="section">
            <p>
              Let's{" "}
              <a target="blank" href="https://lytebulb1.typeform.com/to/AsMExw">
                work together!
              </a>{" "}
              Become a client, collaborator, or friend!
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutLanding
