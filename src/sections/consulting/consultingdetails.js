import React from "react"
import "./consulting.scss"

import FullStackLogos from "../../../static/FullStackLogos.svg"
import LandingLogos from "../../../static/LandingLogos.svg"
import UXLogos from "../../../static/UXLogos.svg"

const ConsultingDetails = () => {
  return (
    <div className="consulting">
      <div className="section">
        <p>
          As a full-stack product manager, I help businesses of all shapes and
          sizes with digitizing, revamping, and building websites and apps.
        </p>
        <p>Here are some ways we can work together.</p>
        <br/> <br/>
        <div className="columns">
          <div className="column">
            <h5 className="title is-5">Full Stack Apps</h5>
            <p>Let's create, manage and deploy full stack apps for mobile and web from scratch.</p>
            <img className="toolLogoFull" src={FullStackLogos} alt="" />
          </div>
          <div className="column">
            <h5 className="title is-5">Blogs and Websites</h5>
            <p>Get your brand out there with a custom built landing page, blog, or ecommerce site, often paired with a CMS like Wordpress.</p>
            <img className="toolLogo" src={LandingLogos} alt="" />          
          </div>
          <div className="column">
            <h5 className="title is-5">UX Design</h5>
            <p>Design new features, prototype an idea, or start an app from scratch, beginning with the design.</p>
            <img className="toolLogo" src={UXLogos} alt="" />
          </div>
          <div className="column">
            <h5 className="title is-5">Consultation</h5>
            <p>Get product management advice to automate something, improve a feature, or something totally different. If it involves problem solving, business, and software, get in touch.</p>
          </div>
        </div>
      </div>
      {/* <div className="section">
        <h5 className="title is-5">Trusted by great companies</h5>
        <p>Glucose Trail</p>
      </div> */}
      <div className="section">
        <h5 className="title is-5">Let's get something going</h5>
        <p>I'm <b><a target="blank" href="https://lytebulb1.typeform.com/to/AsMExw">available</a></b> to take on new projects</p>
      </div>
    </div>
  )
}

export default ConsultingDetails
