import React from 'react';
import './consulting.scss';
import ConsultingScene from '../../../static/consultingScene.svg';

const ConsultingLanding = () => {
    return(
        <div className="section consulting">
            <h2 className="title is-2 bold"><b>Lytebulb Consulting</b></h2>
            <p className="tagline">Websites & Apps - software projects big and small</p>
            <img className="scene" src={ConsultingScene} alt=""/>
        </div>
    )
}

export default ConsultingLanding;