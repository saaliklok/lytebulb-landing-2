import React from "react"
import { Link } from "gatsby"
import "./landingprojects.scss"

import FullLogo from "../../../static/FullLogo.svg"
import LandingScene from "../../../static/landingScene.svg"

const FrontLanding = () => {
  return (
    <div className="frontlanding">
      <div className="container">
        <div className="columns">
          <div className="column">
            <img className="logo" src={FullLogo} alt="" />
            <h5 className="title light">
              A mini product studio creating tech for dreamers and doers.
            </h5>
            <Link to="/about" className="bySaalik">By Saalik Lokhandwala</Link>
          </div>
          <div className="column">
            <img className="landingScene" src={LandingScene} alt="" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default FrontLanding
