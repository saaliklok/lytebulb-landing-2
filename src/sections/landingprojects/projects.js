import React from "react"
import "./landingprojects.scss"
import { CSSTransition, TransitionGroup } from "react-transition-group"
import {
  miniTools,
  fireContent,
  codeExperiments,
  comingSoon,
} from "../../utils/projects"
import ProjectCard from "../../components/projectCard"
//import CardPop from "../../transitions/cardPop"

class Projects extends React.Component {
  state = {
    projectSelection: miniTools,
    fade: false,
  }

  switchList = project => {
    this.setState({
      projectSelection: project,
      fade: false,
    }, () => {
      this.setState({
        fade: true
      })
    })
  }

  render() {
    return (
      <div className="container projectSection">
        <h3 className="title is-3">Projects</h3>

        {/* Buttons displaying different categories. Clicking a button activates that category */}
        <div className="columns is-centered">
          <div className="column">
            <a
              className="button projectSelect"
              onClick={() => this.switchList(miniTools)}
            >
              🧞‍♂️ Mini Web Tools
            </a>
            <a
              className="button projectSelect"
              onClick={() => this.switchList(fireContent)}
            >
              🔥Content
            </a>
            <a
              className="button projectSelect"
              onClick={() => this.switchList(codeExperiments)}
            >
              👨‍🔬Code Experiments
            </a>
            <a
              className="button projectSelect"
              onClick={() => this.switchList(comingSoon)}
            >
              🚀Coming Soon
            </a>
          </div>
        </div>

        {/* Section of Projects */}
        <CSSTransition
          in={this.state.fade}
          timeout={500}
          classNames="card-fade"
        >
          <div className="section">
            <div className="columns is-multiline is-centered">
                {this.state.projectSelection.map((project, index) => (
                  <div className="column is-one-third" key={index}>
                    <ProjectCard
                      title={project.title}
                      tags={project.tags}
                      tagline={project.tagline}
                      actionLink={project.actionLink}
                    />
                  </div>
                ))}
            </div>
          </div>
        </CSSTransition>
      </div>
    )
  }
}

export default Projects
