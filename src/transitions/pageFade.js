import React from 'react';
import {CSSTransition} from 'react-transition-group';
import './transitions.scss';

const PageFade = (props) => {
    return(
      <CSSTransition
        mountOnEnter
        unmountOnExit
        classNames="page"
        appear
        in={true}>
        <div>
          {props.children}
        </div>
      </CSSTransition>
    )
}

export default PageFade;