import React from "react"
import { CSSTransition } from "react-transition-group"
import "./transitions.scss"

const CardPop = props => {
  return (
    <CSSTransition
      key={props.key}
      in={props.fadeIn}
      timeout={500}
      classNames="card-fade"
    >
      <div>{props.children}</div>
    </CSSTransition>
  )
}

export default CardPop
