/**
 * The 'data structure' for the projects:
 * 
 * title: title of project
 * tagline: project tagline
 * actionLink: link
 * tages: array of tags to display
 * 
 */

export const miniTools = [
    {
        title: 'Cognizance',
        tagline: 'Wallpapers designed to wake you up and make you present to the world around you.',
        actionLink: 'https://cognizance.lytebulb.tech',
        tags: ['Wallpapers', 'Web App']
    },
    {
        title: 'Idea Leaf',
        tagline: 'Your business idea, captured on a page for you to share with others or store for yourself.',
        actionLink: 'http://bit.ly/idealeaf',
        tags: ['Ideas', 'Web App']
    },
    {
        title: 'Epoch Clock',
        tagline: 'A simple live clock for use on big screens.',
        actionLink: 'http://epoch-clock.surge.sh',
        tags: ['Clock', 'Web App']
    },
    {
        title: 'VentLetter',
        tagline: 'Write a VentLetter and get your anger out.',
        actionLink: 'https://saaliklok.github.io/VentLetter',
        tags: ['Podcast', 'Mindfulness']
    },
];

export const fireContent = [
    {
        title: 'Slick & Shiv',
        tagline: 'A podcast: Casual conversations on mindfulness, hosted by Saalik and Shivank',
        actionLink: 'http://slickandshiv.lytebulb.tech/',
        tags: ['Podcast', 'Mindfulness']
    },
    {
        title: 'Centerbox',
        tagline: 'Build a toolbox of activities to bring you back to your authentic self',
        actionLink: 'https://centerbox.lytebulb.tech',
        tags: ['Workshop', 'Web']
    },
] 

export const codeExperiments = [
    {
        title: 'Galaxy Gleam',
        tagline: 'Dodge asteroids in this browser game',
        actionLink: 'https://github.com/SaalikLok/GalaxyGleam',
        tags: ['TypeScript', 'PhaserJS']
    },
    {
        title: 'QuoteCamp',
        tagline: 'A crowdsourced catalog of quotes',
        actionLink: 'https://github.com/SaalikLok/QuoteCamp',
        tags: ['Flask', 'Python']
    },
    {
        title: 'Global Adventure Map',
        tagline: 'A web app map of places I visited.',
        actionLink: 'https://github.com/SaalikLok/Global-Adventures-Map',
        tags: ['Mapbox']
    },
    {
        title: 'Samosa Clickers',
        tagline: 'A clicker game with a live leaderboard',
        actionLink: 'https://github.com/SaalikLok/samosa-clickers',
        tags: ['ReactJS']
    },
    
    
]

export const comingSoon = [
    {
        title: 'Runway Finance',
        tagline: 'A simple framework and app to get you to financial clarity.',
        actionLink: 'https://runwayfinance.co',
        tags: ['Finance', 'Late 2019']
    },
]


/**
 * Coming Up:
 * {
        title: 'Lyteletter',
        imgsrc: '',
        tagline: '',
        releaseDate: '',
        status: 'comingSoon',
        actionLabels: ['Check It Out'],
        actionLinks: ['https://cognizance.lytebulb.tech']
    },
 */