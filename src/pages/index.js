import React from "react"
import {Helmet} from 'react-helmet';
import "./global.scss"

import LogoYellow from "../../static/LogomarkYellow.svg"

import Nav from "../components/nav"
import FrontLanding from "../sections/landingprojects/frontlanding"
import Projects from "../sections/landingprojects/projects"
import Footer from "../components/footer"
import PageFade from "../transitions/pageFade";

const IndexPage = () => {
  return (
    <PageFade>
      <Helmet>
        <title>Home | Lytebulb</title>
      </Helmet>
      <Nav page="index" navbar="navbar darknav" logo={LogoYellow} />
      <FrontLanding />
      <Projects />
      <Footer />
    </PageFade>
  )
}

export default IndexPage
