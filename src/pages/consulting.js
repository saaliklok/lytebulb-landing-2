import React from 'react';
import {Helmet} from 'react-helmet';
import './global.scss';

import LogoGrey from "../../static/LogomarkGrey.svg";

import Nav from '../components/nav';
import ConsultingLanding from '../sections/consulting/consultinglanding';
import ConsultingDetails from '../sections/consulting/consultingdetails';
import Footer from '../components/footer';

const ConsultingPage = () => {
    return(
        <div className="consulting">
            <Helmet>
                <title>Consulting | Lytebulb</title>
            </Helmet>
            <Nav page="consulting" navbar="navbar lightnav" logo={LogoGrey}/>
            <div className="container">
                <ConsultingLanding/>
                <ConsultingDetails/>
                <Footer/>
            </div>
        </div>
    )
}

export default ConsultingPage;