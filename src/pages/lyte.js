import React from "react"
import { navigate } from "gatsby"

import "./global.scss"

import logomark from "../../static/LogomarkLight.svg"

class Lyte extends React.Component {
  componentDidMount = () => {
    this.navtoconsulting();
  }

  navtoconsulting = () => {
    setTimeout(() => {
        navigate("/consulting")
    }, 1490)
  }

  render() {
    return (
      <div className="darklightback">
        <img src={logomark} className="bulbcenter" />
      </div>
    )
  }
}

export default Lyte
