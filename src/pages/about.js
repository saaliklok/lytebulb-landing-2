import React from 'react';
import {Helmet} from 'react-helmet';
import './global.scss';
import '../sections/about/about.scss'

import LogoGrey from "../../static/LogomarkGrey.svg";

import AboutLanding from '../sections/about/aboutlanding';
import Nav from '../components/nav';
import Footer from '../components/footer';

const AboutPage = () => {
    return(
        <div>
            <Helmet>
                <title>About | Lytebulb</title>
            </Helmet>
            <Nav page="about" navbar="navbar darknav" logo={LogoGrey}/>
            <AboutLanding/>
            <Footer/>
        </div>
    )
}

export default AboutPage;